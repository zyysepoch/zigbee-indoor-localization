## Zigbee RSSI定位
[Experimental Analysis of RSSI-based Indoor Localization with IEEE 802.15.4](https://gitee.com/zyysepoch/zigbee-indoor-localization/blob/master/Experimental_analysis_of_RSSI-based_indoor_localization_with_IEEE_802.15.4.pdf) pdf版也放仓库里了，点链接直接下载，源网页在reference里跳转。

我们的硬件能向下兼容这个文章里的硬件平台，思路一致，但效果天差地别。  
<div align=center> <img src="https://gitee.com/zyysepoch/chart-bed/raw/master/img/20220822203356.png" alt="rssi与db" width="60%" height="60%"/> </div>  
这个大趋势符合弗里斯传输方程。 

#### 问题分析
咱们那个为啥不行呢，有以下可能性：
1） Xbee质量差
2）串口消息过了一遍Python出现了问题（但可能性很小，真的只是调用了串口而已）。我们使用
```python
XbeeDevice.get_parameter("DB")
```
**但是** 分析着分析着我发现我们的单组数据是没啥问题的

获取RSSI值，经过多次实验，并观察上图的参考值，一定程度上可以说明我们得到的下表数值的确是RSSI，就是每一组同一位置的DB有一定差别。
<div align=center> <img src="https://gitee.com/zyysepoch/chart-bed/raw/master/img/20220822205429.png" alt="rssi与db" width="60%" height="60%"/> </div>  

从图中曲线（来自于论文）可以观察到：5、6、7米几乎不变，在8-14米发生反复横跳。 
从表中数据（来自于咱们的实验）可以观察到：1、2、3、5米单调，5、6平稳，4米处横跳，由此得出结论，我们的单组数据是正常的；而每组只有**1米处数据差别极大**。

*https://github.com/digidotcom/xbee-python/issues/37 此链接中使用另一个API调用RSSI,感兴趣可以尝试*
```python
digi.xbee.packets.base import DictKeys
```

3）基本排除了2）的问题后，那还剩一个可能性极大的原因——环境问题，北京理工大学良乡南校区疏桐园B座北京书院社区社团活动室有什么东西干扰比较大，这也是正常现象。

#### 瓶颈
之前找到的计算方法非常依赖1米处rssi值
<div align=center> <img src="https://gitee.com/zyysepoch/chart-bed/raw/master/img/20220822212944.png" alt="rssi与db" width="80%" height="80%"/> </div>  
所以我们需要看看有无其他方法

#### 对数插值
因为A是d=1m时的信号强度，所以一般做法就是直接测1m处RSSI，但文章中告诉我们了，**The A parameter should be know a priori since it only depends on the physical properties of the radio device.** 直接测不靠谱，咱们每次测的值都不一样还差很多更不行，每个参考Xbee节点的RSSI-距离算式的系数是不同的，需要挨个测。

文章[1]中采用对数插值:

$d=10^{\frac{A-RSSI}{10 · n}}$

d取1-15m，每个距离测25个RSSI值。

#### 下一步
1.继续用真实测量数据进行实验。目前已经测了lga的节点、wwq的节点、还差一个作为参考节点。
2.仿真实验，想用[contiki+cooja模拟](https://www.zhihu.com/question/48708549/answer/139050874)

#### contiki+cooja模拟
下载Instant Contiki 3.0虚拟机。
进入后终端cd contiki-3.0/tools/cooja, 然后ant run 启动cooja。  

问题1：在我用的笔记本上符号“/”，是用Shift+8打出来的，而符号“-”是用/打出来的，键盘编码好像有问题，不过是小问题。

问题2：报错  
BUILD FAILED   
/home/user/contiki-3.0/tools/cooja/build.xml:199: The following error occurred while executing this line: /home/user/contiki-3.0/tools/cooja/apps/mspsim/build.xml:29: - Could not find the MSPSim build file. Did you run "git submodule update --init"?
解决：按照[博客](https://blog.csdn.net/u014070086/article/details/80666399)中的方法完美解决，成功运行了。

开启学习之路
http://anrg.usc.edu/contiki/index.php/Contiki_tutorials 有一节是讲RSS的，试了之后运行结果显然不对，正在搜索解决方法
[Cooja Simulator Manual](https://www.researchgate.net/publication/304572240_Cooja_Simulator_Manual) 操作更加详细，但没有RSS相关信息

#### Reference
[1][Experimental Analysis of RSSI-based Indoor Localization with IEEE 802.15.4](https://ieeexplore.ieee.org/document/5483396) 


